package main

import (
	"time"
	"strconv"
)

func ParseUnixTime(timeString string) (time.Time, error) {
	i, err := strconv.ParseInt(timeString, 10, 64);

	var t time.Time
	if (err != nil) {
		t = time.Unix(0, 0)
	} else {
		t = time.Unix(i, 0)
	}

	return t, nil
}
