package main

import (
	"testing"
	"time"
)

func AssertIntermediateObject(t *testing.T, got, want IntermediateObject) {
	t.Helper()
	for j, _ := range want {
		if got[j] != want[j] {
			t.Errorf("got %q want %q", got[j], want[j])
		}
	}
}

func AssertMowtObject(t *testing.T, got, want MowtObject) {
	t.Helper()
	for i, _ := range want {
		if got[i] == nil {
			t.Errorf("got nil map")
		}

		for j, _ := range want[i] {
			if got[i][j] != want[i][j] {
				t.Errorf("got %d, want %d for indexes %d, %d", got[i][j], want[i][j], i, j)
			}
		}
	}
}

func MatchSlot(gotMowt, wantMowt MowtJson, wantSlot TimeSlot) TimeSlot {
	for _, gotSlot := range gotMowt.TimeSlots {
		if gotSlot.Start == wantSlot.Start {
			return gotSlot
		}
	}
	return TimeSlot { -1, -1, -1 }
}

func MatchMowtJson(got []MowtJson, wantMowt MowtJson) MowtJson {
	for _, gotMowtJson := range got {
		if gotMowtJson.Timestamp == wantMowt.Timestamp {
			return gotMowtJson
		}
	}
	return MowtJson { time.Unix(int64(0), 0), []TimeSlot {} }
}

func IsBadSlot(slot TimeSlot) bool {
	return slot.Start == -1
}

func IsBadMowtJson(mowtJson MowtJson) bool {
	return mowtJson.Timestamp.Unix() == 0
}

func AssertSlotSize(t *testing.T, wantMowt MowtJson, wantSlot, gotSlot TimeSlot) {
	if wantSlot.Size != gotSlot.Size {
		t.Errorf("timestamp %d, start %d:  expected size value of %d but got %d",
			wantMowt.Timestamp.Unix(),
			wantSlot.Start,
			wantSlot.Size,
			gotSlot.Size,
		)
	}
}

func AssertSlotWorkspaceId(t *testing.T, wantMowt MowtJson, wantSlot, gotSlot TimeSlot) {
	if wantSlot.WorkspaceId != gotSlot.WorkspaceId {
		t.Errorf("timestamp %d, start %d:  expected workspaceId value of %d but got %d",
			wantMowt.Timestamp.Unix(),
			wantSlot.Start,
			wantSlot.WorkspaceId,
			gotSlot.WorkspaceId,
		)
	}
}

func AssertMowtJson(t *testing.T, got, want []MowtJson) {
	t.Helper()

	for _, wantMowtJson := range want {
		gotMowtJson := MatchMowtJson(got, wantMowtJson)
		if IsBadMowtJson(gotMowtJson) {
			t.Errorf("timestamp %d not found",
				wantMowtJson.Timestamp.Unix())
		}
		if gotMowtJson.Timestamp == wantMowtJson.Timestamp {
			for _, wantSlot := range wantMowtJson.TimeSlots {
				gotSlot := MatchSlot(gotMowtJson, wantMowtJson, wantSlot)
				if IsBadSlot(gotSlot) {
					t.Errorf("timestamp %d, start %d: not found",
						wantMowtJson.Timestamp.Unix(),
						wantSlot.Start,
					)
					break
				}
				AssertSlotSize(t, wantMowtJson, wantSlot, gotSlot)
				AssertSlotWorkspaceId(t, wantMowtJson, wantSlot, gotSlot)
			}
		}
	}
}

func TestGetFileAsString(t *testing.T) {
	got := GetFileAsString("./sample.log")
	want:= "1601696945\n0.\n2.\n0................................................................................................................................\n1603491270\n0....\n1603491457\n0.........................\n2..\n0.........\n1.\n0.............................\n1.\n0....................................\n1.\n0..\n1.\n0..\n1...\n0..\n2.............................\n0.\n1..."

	AssertString(t, got, want)
}

func TestParseIntermediateObject(t *testing.T) {
	got := ParseIntermediateObject("1601696945\n0.\n2.\n0................................................................................................................................\n1603491270\n0....\n1603491457\n0.........................\n2..\n0.........\n1.\n0.............................\n1.\n0....................................\n1.\n0..\n1.\n0..\n1...\n0..\n2.............................\n0.\n1...")
	want := IntermediateObject {
		1601696945: "\n0.\n2.\n0................................................................................................................................\n",
		1603491270: "\n0....\n",
		1603491457: "\n0.........................\n2..\n0.........\n1.\n0.............................\n1.\n0....................................\n1.\n0..\n1.\n0..\n1...\n0..\n2.............................\n0.\n1...",
	}

	AssertIntermediateObject(t, got, want)
}

func TestSimpleParseMowtObjects(t *testing.T) {
	got := ParseMowtObject(IntermediateObject {
		1601696945: "\n0.\n2.\n",
		1603491457: "\n0..\n2..\n",
	})

	want := MowtObject {
		1601696945: map[int] int{
			1: 0,
			2: 2,
		},
		1603491457: map[int] int{
			2: 0,
			4: 2,
		},
	}

	AssertMowtObject(t, got, want)
}

func TestParseMowtObjects(t *testing.T) {
	got := ParseMowtObject(IntermediateObject {
		1601696945: "\n0.\n2.\n0................................................................................................................................\n",
		1603491270: "\n0....\n",
		1603491457: "\n0.........................\n2..\n0.........\n1.\n0.............................\n1.\n0....................................\n1.\n0..\n1.\n0..\n1...\n0..\n2.............................\n0.\n1...",
	})

	want := MowtObject {
		1601696945: map[int] int{
			1: 0,
			2: 2,
			130: 0,
		},
		1603491270: map[int] int{
			4: 0,

		},
		1603491457: map[int] int{
			25: 0,
			27: 2,
			36: 0,
			37: 1,
			66: 0,
			67: 1,
			103: 0,
			104: 1,
			106: 0,
			107: 1,
			109: 0,
			112: 1,
			114: 0,
			143: 2,
			144: 0,
			147: 1,
		},
	}

	AssertMowtObject(t, got, want)
}

func TestMowtObjectToMowtJson(t *testing.T) {
	got := MowtObjectToMowtJson(MowtObject {
		1601696945: map[int] int{
			1: 0,
			2: 2,
			130: 0,
		},
		1603491270: map[int] int{
			4: 0,
		},
		1603491457: map[int] int{
			25: 0,
			27: 2,
			36: 0,
			37: 1,
			66: 0,
			67: 1,
			103: 0,
			104: 1,
			106: 0,
			107: 1,
			109: 0,
			112: 1,
			114: 0,
			143: 2,
			144: 0,
			147: 1,
		},
	})

	want := []MowtJson {
	{
	  time.Unix(int64(1601696945), -3),
			[]TimeSlot {
		    TimeSlot { 0, 0, 1 },
			  TimeSlot { 2, 1, 1 },
			  TimeSlot { 0, 2, 128 },
			},
	},
	{
		time.Unix(int64(1603491270), -3),
			[]TimeSlot {
			  TimeSlot { 0, 0, 4 },
			},
  },
	{
		time.Unix(int64(1603491457), -3),
		  []TimeSlot {
			  TimeSlot { 0, 0, 25 },
				TimeSlot { 2, 25, 2 },
			  TimeSlot { 0, 27, 9 },
				TimeSlot { 1, 36, 1 },
				TimeSlot { 0, 37, 29 },
				TimeSlot { 1, 66, 1 },
				TimeSlot { 0, 67, 36 },
				TimeSlot { 1, 103, 1 },
				TimeSlot { 0, 104, 2 },
				TimeSlot { 1, 106, 1 },
				TimeSlot { 0, 107, 2 },
				TimeSlot { 1, 109, 3 },
				TimeSlot { 0, 112, 2 },
				TimeSlot { 2, 114, 29 },
				TimeSlot { 0, 143, 1 },
				TimeSlot { 1, 144, 3 },
			},
		},
	}

	AssertMowtJson(t, got, want)
}
