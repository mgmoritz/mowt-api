package main

func FilterString (s []string, filter func(string) bool) []string {
	var r []string
	for _, str := range s {
		if (filter(str)) {
			r = append(r, str)
		}
	}
	return r
}

func FilterEmpty (str string) bool{
	return str != ""
}
