package main

import (
	"testing"
)

func TestFilterString(t *testing.T) {
	got := FilterString([]string { "1", "", "", "2", "", "3"}, FilterEmpty)
	want := []string { "1", "2", "3"}

	AssertStringArray(t, got, want)
}

func AssertString(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}

func AssertStringArray(t *testing.T, got, want []string) {
	t.Helper()
	for i := 0; i < len(want); i++ {
		if got[i] != want[i] {
			t.Errorf("got %q want %q", got[i], want[i])
		}
	}
}
