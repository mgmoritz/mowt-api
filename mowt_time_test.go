package main

import (
	"testing"
	"time"
)


func TestParseUnixTime(t *testing.T) {
	got, err := ParseUnixTime("1601696945")
	if err != nil { t.Fail() }
	want:= time.Unix(1601696945, 0)

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
