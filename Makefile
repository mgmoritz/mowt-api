.PHONY: all build run

all: build run

build: mowt.go
	go build

run: mowt
	go run mowt
