package main

import (
	"encoding/json"
	"regexp"
	"log"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
	"sort"
)

type IntermediateObject map[int64] string
type MowtObject map[int64] map[int] int


type TimeSlot struct {
	WorkspaceId int `json:"workspaceId"`
	Start int `json:"start"`
	Size int `json:"size"`
}

type MowtJson struct {
	Timestamp time.Time `json:"timestamp"`
	TimeSlots []TimeSlot `json:"timeSlots"`
}

func main() {
	http.HandleFunc("/", GetMowt)
	http.ListenAndServe(":8080", nil)
}

func SplitMowtChunks(fileString string) []string {
	unixEpochRegexpString := "[0-9]{10}"
	reg := regexp.MustCompile(unixEpochRegexpString)
	timestamps := reg.FindAllString(fileString, -1)
	dotGroups := RegSplit(fileString, reg)[1:]
	for i:=0; i<len(timestamps); i++ {
		timestamps[i] += dotGroups[i]
	}
	return timestamps
}

func ParseIntermediateObject(fileString string) IntermediateObject {
	unixEpochRegexpString := "[0-9]{10}"
	reg := regexp.MustCompile(unixEpochRegexpString)
	timestamps := reg.FindAllString(fileString, -1)
	dotGroups := RegSplit(fileString, reg)[1:]
	var lines = make(IntermediateObject)

	for i:=0; i<len(timestamps); i++ {
		timeInt, err := strconv.ParseInt(timestamps[i], 10, 64)
		if err != nil {
			log.Fatal()
		}
		lines[timeInt] = dotGroups[i]
	}
	return lines
}

func ParseMowtObject(mowtIntermediateObject IntermediateObject) MowtObject {
	var result = make(MowtObject)
	for j, _ := range mowtIntermediateObject {
		lines := FilterString(strings.Split(mowtIntermediateObject[j], "\n"), FilterEmpty)
		var timeSlot = make(map[int]int)
		var acc = 0
		for _, line := range lines {
			acc += len(line) -1
			workspaceId, err := strconv.Atoi(line[0:1])
			if (err != nil) {
				log.Fatal()
			}
			timeSlot[acc] = workspaceId
		}

		result[j] = timeSlot
	}
	return result
}

func RegSplit(text string, reg *regexp.Regexp) []string {
    indexes := reg.FindAllStringIndex(text, -1)
    laststart := 0
    result := make([]string, len(indexes) + 1)
    for i, element := range indexes {
            result[i] = text[laststart:element[0]]
            laststart = element[1]
    }
    result[len(indexes)] = text[laststart:len(text)]
    return result
}

// {"1604249412":{"1":0,"3":2,"5":3},"1604250317":{"1":0},"1604250388":{"1":0}}

func MowtObjectHasValue(rawTimes map[int] int, acc int) int {
	if value, ok := rawTimes[acc]; ok {
		return value
	}
	return -1
}

func MowtObjectToMowtJson(mowtObject MowtObject) []MowtJson {
	mjson := []MowtJson {}

	for t, _ := range mowtObject {

		mjson = append(mjson, MowtJson { time.Unix(t, -3), [] TimeSlot{}})
		acc := 0

		var keys []int
		for k := range mowtObject[t] {
			keys = append(keys, k)
		}
		sort.Ints(keys)
		for _, k := range keys {
			workspaceId := mowtObject[t][k]
			start := acc
			size := k - acc
			last := len(mjson) - 1
			mjson[last].TimeSlots = append(mjson[last].TimeSlots,
				TimeSlot {
				  workspaceId,
					start,
				  size,
				})
			acc = k
		}
		// for k, v := range mowtObject[t] {
		// 	last := len(mjson) - 1
		// 	mjson[last].TimeSlots = append(mjson[last].TimeSlots,
		// 		TimeSlot {
		// 		  v,
		// 		  acc,
		// 		  (k - acc),
		// 		})
		// 	acc = (k - acc)
		// }
	}

	return mjson
}

func MowtToJson(mowt MowtObject) []byte {
	return nil
}

func GetMowt(w http.ResponseWriter, r *http.Request) {
	fileString := GetFileAsString("/home/moritz/.workspace-log")
	intermediateObject := ParseIntermediateObject(fileString)
	mowtObject := ParseMowtObject(intermediateObject)
	mowtJsonStruct := MowtObjectToMowtJson(mowtObject)
	mowtJson, _ := json.Marshal(mowtJsonStruct)
	// mowtJson, _ := json.Marshal(mowtObject)


	w.Header().Set("Content-Type", "application/json")
	w.Write(mowtJson)
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func GetFileAsString(filename string) string {
	dat, err := ioutil.ReadFile(filename)
	check(err)
	return string(dat)
}
