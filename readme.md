# Moritz Workspace Time

This is a toy project to track the time spent in each workspace
in Gnome 3.

This is composed by 3 projects:


- Mowt Logger (C++)
- Mowt API (GoLang)
- Mowt Client (Vue.js?)


# Mowt Logger

C++ project that create logs the time in a workspace in a file
in $HOME/.workspace-log

The current format of the data is:


## Structure


```
<unix-timestamp-for-session-start>
<workspace-number><one-dot-for-each-minute-in-this-workspace>
```

## Real example


```
1601696945
0.
2.
0..
2.....
0.
```

[Mowt Logger git repository](git@gitlab.com:mgmoritz/mowt-logger.git "Mowt logger git repository")


# Mowt API


A GoLang API that parses the log file and answers to http requests

## Json Format

Currently the only route is the root path and it answers with the whole file content


```json
[
  {
    "timeSlots" : [
      {
        "workspaceId" : 0,
        "size" : 29,
        "start" : 0
      },
      {
        "workspaceId" : 2,
        "size" : 109,
        "start" : 29
      }
    ],
    "timestamp" : "2020-10-14T22:21:40.999999997-03:00"
  },
  [
    {
      "timeSlots" : [
        {
          "workspaceId" : 0,
          "size" : 29,
          "start" : 0
        }
      ],
      "timestamp" : "2020-10-14T22:21:40.999999997-03:00"
    }
  ]
]
```

# Mowt Client

TBD
